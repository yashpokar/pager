import sys
from setuptools import setup, find_packages

REQUIRED_PYTHON = (3, 5)
CURRENT_PYTHON = sys.version_info[:2]

if CURRENT_PYTHON < REQUIRED_PYTHON:
    sys.stderr.write(("\n"
                      "=====================================\n"
                      "Unsupported Python version\n"
                      "=====================================\n"
                      "This version of il-operation requires Python {}.{}, but you're trying to\n"
                      "install it on Python {}.{}.\n").format(*(REQUIRED_PYTHON + CURRENT_PYTHON)))
    sys.exit(1)
setup(
    name='pager',
    description='Pager system',
    version='0.0.1-dev',
    include_package_data=True,
    python_requires='>=3.5',
    install_requires=(
    	'Flask',
    	'Flask-SQLAlchemy',
    	'Flask-Migrate',
        'Flask-WTF',
    	'pymysql',
    	'python-dotenv',
        'requests',
    ),
    packages=find_packages(exclude=('tests', 'tests/*')),
    license='MIT',
    zip_safe=False,
    test_suite='nose.collector',
    tests_require=['nose'],
)

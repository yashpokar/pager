

class TestTeamCreationAPI():
	def test_team_api_creation_in_happy_path():
		data = {
		    "team": {
		        "name": "claims"
		    },
		    "developers": [
		        {
		            "name": "someone",
		            "phone_number": "9999999999"
		        },
		        {
		            "name": "somebody",
		            "phone_number": "9111111111"
		        }
		    ]
		}

		response = self.request.post('/team/create', json=data)
		response_data = response.json()

		self.assertEqual(200, response.status_code)
		self.assertEqual(response_data['message'], 'Team is created successfully.')

	def test_team_api_should_fail_when_invalid_input_supplied(self):
		data = {
		    "team": {
		    },
		    "developers": [
		        {
		            "name": "someone",
		            "phone_number": "9999999999"
		        },
		        {
		            "name": "somebody",
		            "phone_number": "9111111111"
		        }
		    ]
		}

		response = self.request.post('/team/create', json=data)
		response_data = response.json()

		self.assertEqual(422, response.status_code)
		self.assertEqual(response_data['message'], 'Validation failed.')

	def test_team_api_when_duplicate_submition_is_happening(self):
		# TODO :: request twice with the same playload and assert last request response

	def check_if_database_insertion_happned():
		pass

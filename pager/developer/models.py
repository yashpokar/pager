from .. import db


class Developer(db.Model):
	__tablename__ = 'developers'

	id = db.Column(db.Integer, primary_key=True)
	team_id = db.Column(db.Integer, db.ForeignKey('teams.id'), nullable=False)
	name = db.Column(db.String(255))
	phone = db.Column('phone_number', db.String(10), nullable=False, unique=True)

	team = db.relationship('Team', back_populates='developers')

DEBUG = True
ENV = 'development'
SECRET_KEY = b'some-secret-key-here'

# TODO :: these should come from environment specific file but leaving hardcoded
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://pager:pager@localhost/pager'
SQLALCHEMY_TRACK_MODIFICATIONS = False

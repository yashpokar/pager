from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_wtf.csrf import CSRFProtect

db = SQLAlchemy()
migrate = Migrate(db=db)
csrf = CSRFProtect()

def create_app(config):
	app = Flask(__name__)
	app.config.from_object(config)

	db.init_app(app)
	migrate.init_app(app)
	csrf.init_app(app)

	from .team.views import team
	from .developer.views import developer

	app.register_blueprint(team)
	app.register_blueprint(developer)

	return app

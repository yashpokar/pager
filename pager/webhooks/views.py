from flask import Blueprint, jsonify, request
import requests
import random

from ..team.models import Team

webhook = Blueprint('webhook', __name__)

@webhook.route('/notify', methods=['POST'])
def notify_alert():
	team_id = request.json.get('team_id')

	if not team_id:
		return jsonify(success=False, message='Team id is required.'), 422

	team = Team.query.filter_by(id=team_id).first()

	if not team:
		return jsonify(success=False, message='No such team exists.'), 422

	developer = random.choice(team.developers)

	try:
		response = requests.post('https://run.mocky.io/v3/fd99c100-f88a-4d70-aaf7-393dbbd5d99f', json=dict(phone_number=developer.phone, message='Too many 5xx!'))

		print(response.json())
		# TODO :: check if the response was success, or else reply failure

		# TimeoutError
	except Exception as e:
		pass

	return jsonify(success=True, message='Notified to the developer')

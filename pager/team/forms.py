import re
from flask_wtf import FlaskForm
from wtforms import StringField, FormField, FieldList
from wtforms.validators import DataRequired, Length, \
	ValidationError


# TODO :: not a right place to write this, because this is a utility class
class ValidatePhoneNumber(object):
	VALIDATOR = re.compile(r'\d{10}')

	def __call__(self, form, field):
		if not re.fullmatch(field.data):
			raise ValidationError('Invalid phone number supplied.')

		return True

class TeamDetailsForm(FlaskForm):
	name = StringField('name', validators=[
		DataRequired(message='Team name is required.'),
		Length(max=255, message='Team name should not have greater than 255 characters.'),
		# TODO :: add not exists validation
	])


class DeveloperDetailsForm(FlaskForm):
	name = StringField('name', validators=[
		DataRequired(message='Developer name is required.'),
		Length(max=255, message='Developer name should not have greater than 255 characters.'),
	])
	phone = StringField('phone_number', validators=[
		DataRequired(message='Developer\'s phone number is required.'),
		ValidatePhoneNumber(),
		# TODO :: add not exists validation
	])


# class TeamCreationForm(FlaskForm):
# 	team = FieldList('team', FormField(TeamDetailsForm), min_entries=1, max_entries=1)
# 	developers = FieldList('developers', FormField(DeveloperDetailsForm), min_entries=1, message='Atleast one developer has to be there inside a team.')

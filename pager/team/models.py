from .. import db


class Team(db.Model):
	__tablename__ = 'teams'

	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(255), unique=True, nullable=False)

	developers = db.relationship('Developer', back_populates='team')

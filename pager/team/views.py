from flask import Blueprint, jsonify, request

from .. import db
from .models import Team
from ..developer.models import Developer
from .forms import TeamDetailsForm, DeveloperDetailsForm

team = Blueprint('team', __name__)

@team.route('/team/create', methods=['POST'])
def create():
	data = request.json

	form = TeamDetailsForm(data=data.get('team', {}))

	if not form.validate():
		return jsonify(success=False, message='Validation failed.', errors=form.errors), 422

	developers = data.get('developers', [])

	if not developers:
		return jsonify(success=False, message='Atleast one developer is required in the team.')

	for developer in developers:
		form = DeveloperDetailsForm(data=developer)

		if not form.validate():
			return jsonify(success=False, message='Validation failed.', errors=form.errors), 422

	team = Team(name=data['team']['name'])
	# TODO :: loop
	team.developers.add(Developer())

	db.session.add(team)

	return jsonify(success=True, message='Team is created successfully.')

from pager import create_app
import pager.settings

app = create_app(pager.settings)

if __name__ == '__main__':
	app.run(debug=True)
